console.log(`s47 - Introduction to DOM`);

console.log(document);
// result: the document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);

/*
	alternative ways:
		>> document.getElementById("txt-first-name")
		>> document.getElementByClassName("text-class")
		>> document.getElementByTagName("h1")
*/

// targets the full name
const spanFullName = document.querySelector("#span-full-name")

// event listeners
// (event) can have a shorthand of (e)
// Syntax: addEventListener('stringOfAnEvent', function)
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value
});

// multiple listeners
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});